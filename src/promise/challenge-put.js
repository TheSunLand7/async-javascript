const API = 'https://api.escuelajs.co/api/v1';

function updateData(urlApi, data) {
    const response = fetch(urlApi, {
        method: 'PUT',
        mode: 'cors',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    return response;
}

const update = {
    'title': 'Title has changed',
    'price': 2500,
}

updateData(`${API}/products/13`, update)
    .then(response => response.json())
    .then(data => console.log(data))
    .catch(error => console.log(error));