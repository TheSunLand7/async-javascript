const API = 'https://api.escuelajs.co/api/v1';

const fetchData = urlAPI => fetch(urlAPI);

// fetchData(`${API}/products`)
//     .then(response => response.json())
//     .then(products => console.log(products))
//     .catch(error => console.log(error));

fetchData(`${API}/products`)
    .then(response => response.json())
    .then(products => fetchData(`${API}/products/${products[0].id}`)) //Retorna el primer "objeto" del array
    .then(response2 => response2.json()) //Lo convierte a formato json para poder trabajarlo
    .then(product => fetchData(`${API}/categories/${product.category.id}`))
    .then(response3 => response3.json())
    .then(category => console.log(category.name))
    .catch(error => console.log(error))
    .finally(() => console.log('Finally'));