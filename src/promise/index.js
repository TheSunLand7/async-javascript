const COWS = 9;
const promise = new Promise((resolve, reject) => {
    if (COWS > 10) resolve(`We have ${COWS} cows at the farm!`);
    else reject('There are no cows enough at the farm!');
});

promise
    .then(result => console.log(result))
    .catch(error => console.log(error))
    .finally(() => console.log('Finally'));