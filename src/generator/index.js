// function* iterator(array) {
//     for (const item of array) {
//         yield item;
//     }
// }

// const it = iterator(['leo', 'pedro', 'juan', 'maria']);

// console.log(it.next().value);
// console.log(it.next().value);
// console.log(it.next().value);
// console.log(it.next().value);

const API = 'https://api.escuelajs.co/api/v1';

async function fetchData(urlAPI) {
    const response = await fetch(urlAPI);
    const data = await response.json();
    return data;
}

async function* getData(urlAPI) {
    try {
        const products = await fetchData(`${urlAPI}/products`);
        yield products;
        const product = await fetchData(`${urlAPI}/products/${products[0].id}`);
        yield product;
        const category = await fetchData(`${urlAPI}/categories/${product.category.id}`);
        yield category;
    } catch (error) {
        console.log(error);
    }

}
const it = getData(API)
it.next()
    .then(item => {
        console.log(item.value);
    });
it.next()
    .then(item => {
        console.log(item.value.title);
    });
it.next()
    .then(item => {
        console.log(item.value.name);
    })