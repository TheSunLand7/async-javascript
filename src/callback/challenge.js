/** My own way - con funciones anonimas autoejecutables
((() => {
    const xhr = new XMLHttpRequest(),
        API = 'https://api.escuelajs.co/api/v1/products',
        $xhr = document.getElementById('app'),
        $container = document.createDocumentFragment();

    xhr.open('GET', API);
    xhr.addEventListener('readystatechange', (event) => {
        if (xhr.readyState !== 4) return;
        if (xhr.status >= 200 && xhr.status < 300) {
            const json = JSON.parse(xhr.responseText);
            json.forEach(item => {
                const $li = document.createElement('li')
                $li.innerHTML = `
                Title: ${item.title}<br/>
                Price: ${item.price}<br/>
                Category: ${item.category.name}<br/><br/>
                `;
                $container.appendChild($li);
            });
            $xhr.appendChild($container);
        } else console.log(`Error: ${xhr.status}`);
    })
    xhr.send();

})());
*/

const XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
const API = 'https://api.escuelajs.co/api/v1';

const fetchData = (urlAPI, callback) => {
    const xhttp = new XMLHttpRequest(); //

    xhttp.open('GET', urlAPI); //Inicializa una nueva petición o tambien puede reinicializar

    // Es un eventHandler. Es llamado cuando la propiedad readyState cambia
    xhttp.onreadystatechange = function (event) {
        if (xhttp.readyState === 4) { //Retorna el estado de la petición del cliente(nosotros). [0-4];
            if (xhttp.status === 200) { //Retorna el codigo de estado de la respuesta de la petición. (100,200,400,500)
                //Recibe la solicitud que nos esta entregando el llamado a la API (datos)
                callback(null, JSON.parse(xhttp.responseText));
            } else {
                const error = new Error('Error in: ', urlAPI);
                return callback(error, null);
            }
        }
    }
    xhttp.send(); //Enviamos la petición al servidor.
}

fetchData(`${API}/products`, function (error1, data1) {
    if (error1) return console.error(error1);
    fetchData(`${API}/products/${data1[0].id}`, (error2, data2) => {
        if (error2) return console.error(error2);
        fetchData(`${API}/categories/${data2?.category?.id}`, (error3, data3) => {
            if (error3) return console.error(error3);
            console.log(
                data1[0],
                "\n" + data2.title,
                "\n" + data3.name,
            );
        })
    })
})