
const calculadora = [
    function sum(num1, num2) {
        return num1 + num2;
    },

    function rest(num1, num2) {
        return num1 - num2;
    },

    function mult(num1, num2) {
        return num1 * num2;
    },

    function div(num1, num2) {
        return num1 / num2;
    },
];

/**
 * Calc takes two numbers and a callback function, and returns the result of the callback function when
 * called with the two numbers.
 * @param num1 - The first number to be used in the calculation.
 * @param num2 - The second number to be used in the calculation.
 * @param callback - A function that is called after calc is done.
 * @returns The result of the callback function.
 */
function calc({ num1, num2, callback }) {
    return callback(num1, num2);
}

console.log(calculadora.map(item => calc({ num1: 1, num2: 4, callback: item })));

setTimeout(() => {
    console.log('Hola JavaScript');
}, 2000);

function greeting(name) {
    console.log(`Hola ${name}, ¡Bienvenido!`);
}
setTimeout(greeting, 0, 'xiayudev');




/** Example
 * The order function takes two arguments, the first one is a string and the second one is a callback
 * function. The order function will wait 2 seconds, then it will log the string that was passed to it,
 * then it will wait 5 seconds, then it will call the callback function that was passed to it, and then
 * it will wait 9 seconds.
 * @param orden - This is the order that the customer wants.
 */
function makingOrder(orden) {
    console.log(`Your ${orden} is ready! Enjoy it!`);
}
function order(orden, callback) {
    setTimeout(() => {
        console.log(`Taking order: ${orden} ...`);
    }, 2000);
    setTimeout(() => {
        callback(orden)
    }, 9000)
    setTimeout(() => {
        console.log(`Doing order: ${orden} ... Please wait`);
    }, 5000);
}

order('Burger', makingOrder);